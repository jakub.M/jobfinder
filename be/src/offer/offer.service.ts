import { Injectable, HttpService } from '@nestjs/common';
import { includes } from 'lodash';

@Injectable()
export class OfferService {
  constructor(private readonly httpService: HttpService) {}

  findAll = async () =>
  (await this.offers())
    .data
    .filter(this.findConditionFunc)
    .map(this.take(this.fields))

  offers = async () => await this.httpService.get('https://justjoin.it/api/offers').toPromise();

  findConditionFunc = (offer: any) => (
    offer.remote === true
    || includes(offer.address_text, 'Bielsko')
    || includes(offer.address_text, 'Katowice')
  ) && offer.marker_icon === 'javascript'

  fields = ['title', 'address_text', 'remote', 'published_at', 'id'];

  take = (fields: string[]) => (obj: any) =>
    fields.reduce((res, field) => ({ ...res, [field]: obj[field] }), {})
}
