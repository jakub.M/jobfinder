import { Module, HttpModule } from '@nestjs/common';
import { OfferService } from './offer.service';
import { OfferController } from './offer.controller';

@Module({
  imports: [HttpModule.register({
    timeout: 50000,
    maxRedirects: 5,
  })],
  providers: [OfferService],
  controllers: [OfferController],
})
export class OfferModule {}
